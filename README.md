Trata-se de um jogo no qual o servidor seleciona uma palavra com uma quantidade significativa de letras e guarda tambem a quantidade de letras da palavra.
Os jogadores devem dar seu palpite a respeito da palavra (quantas letras a mesma possui), caso o palpite seja correto ou tenha a margem de erro de 1 (UMA) letra para mais ou para menos o jogador vence a rodada, caso nao acerte todos os demais jogadores marcam ponto.
Ao final de 10 rodadas de palavras e palpites aquele jogador que tiver somado mais pontos saira como o vencedor, caso o jogo empate mais duas palavras serao selecionadas para fazer o desempate.

Lembrando que cada alternativa em que voce pontue por ACERTAR a quantidade, soma-se 3 pontos e a cada vez que voce pontue por um ERRO DO ADVERSARIO soma-se 1 ponto.